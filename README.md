# Application - Service Component
[![pipeline status](https://gitlab.com/openfintechlab/kadal0g/application-ora/badges/master/pipeline.svg)](https://gitlab.com/openfintechlab/kadal0g/application-ora/-/commits/master) 

Service component project template for medulla middleware. This project will act as a boilerplate for developing other service components.

## Getting Started
The core purpose of this project is to boot-start the development of the service component by using this bare-bone project library.

## Prerequisites
- Ensure all pre-requisites are met. Refer to the shared folder `Process Catalog > Configuration Management > GitLab`
- Ensure your new service library is ready and you have initialized git repo

| Software  | Version       |
|-----------|:--------------|   
| Node      | 12.17.0 LTS   |
| Kubernetes| v1.18.2       |
| docker    | v19.0.3       |
| VS Code   | Latest        |
| yarn      | 1.22.4        |
| typescript| 3.9.3         |
> **NOTE:** Please note that the versions will keep on changing. 
## How to use the boilerplate project ?
Follow the steps below to download and use the boilerplate code:

### Step#1: Clone the repository
Clone the repository by issuing:
```
$ git clone git@gitlab.com:openfintechlab/medulla/applications/service-boilerplate-api.git
Cloning into 'service-boilerplate-API'...
remote: Enumerating objects: 69, done.
remote: Counting objects: 100% (69/69), done.
remote: Compressing objects: 100% (51/51), done.
remote: Total 69 (delta 27), reused 41 (delta 9), pack-reused 0
Receiving objects: 100% (69/69), 36.89 KiB | 186.00 KiB/s, done.
Resolving deltas: 100% (27/27), done.
```
### Step#2: Fetch the libraries
Fetch all libraries by issuing:
```
$ yarn install
yarn install v1.22.4
[1/5] Validating package.json...
[2/5] Resolving packages...
[3/5] Fetching packages...
info fsevents@2.1.3: The platform "linux" is incompatible with this module.
info "fsevents@2.1.3" is an optional dependency and failed compatibility check. Excluding it from installation.
[4/5] Linking dependencies...
[5/5] Building fresh packages...
Done in 6.75s.
```
### Step#3: Remove `.git` and move the code to the new git repo
Remove `.git` directory and move the content of the folder to your new service component repo directory.
> **NOTE:** This is step will be eliminated once console util for initializing the repo is released.

### Step#4: Modify `package.json` and new service commponent detail
- Edit `package.json` file and add project related details for following tags:
```
"name": "service-boilerplate-api",
  "version": "0.1.0",
  "description": "Boilerplate code for developing Medulla service components",
  "main": "index.js",
  "repository": {
    "type": "git",
    "url": "git+https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api.git"
  },
```
And following snippet as well:
```
"author": "Muhammad Furqan <furqan.baqai@openfintechlab.com>",
  "license": "SEE LICENSE IN LICENSE.md",
  "bugs": {
    "url": "https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api/issues"
  },
  "homepage": "https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api#readme",
```
> NOTE: Add this time, you are ready to use your new service component. Commit the code to your new repository

## Building and Running service component
### Build the project
You can build the project by issuing following commands:
```
$ yarn build
yarn run v1.22.4
$ rm -fr ./bin/* && tsc
Done in 2.16s.
```
### Run the project in development mode
You can run the project by issuing following commands:
```
$ yarn dev
yarn run v1.22.4
$ export OFL_MED_CONTEXT_ROOT='/boiler-plate/' && nodemon ./bin/index.js
[nodemon] 2.0.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node ./bin/index.js`
[2020-06-13T07:43:54.282] [INFO] default - -----------------------------------
[2020-06-13T07:43:54.286] [INFO] default - Medulla - The Financial Middleware
[2020-06-13T07:43:54.286] [INFO] default - Copyright @ Openfintechlab.com
[2020-06-13T07:43:54.286] [INFO] default - -----------------------------------
[2020-06-13T07:43:54.286] [INFO] default - Starting Application
[2020-06-13T07:43:54.290] [INFO] default - Context root of the application: /boiler-plate/
[2020-06-13T07:43:54.290] [INFO] default - App listening on Port: 3000
```

You can access the application using url `http://<url>:<OFL_MED_PORT>/<OFL_MED_CONTEXT_ROOT>/<your-routes-definition>`

> **NOTE:** Hostname and port can change when service component will run as a docker container or kubernetes pod

### Testing pre-defined routes
There are three rules pre-defined in the project:
#### `/healthz` for liveness test
This is a special route and can be used for defining liveness probe. This route will always respond back with response code `200`. This route can be tuned more according to the service component requirement.
```
$ curl http://192.168.159.131:3000/service-boilerplate/healthz -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 3000 (#0)
> GET /service-boilerplate/healthz HTTP/1.1
> Host: 192.168.159.131:3000
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sat, 02 May 2020 20:00:58 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```
#### `/readiness` for readiness test
This is a special route and can be used for defining readiness probe. This route will always respond back with response code `200`. This route can be tuned more according to the service component requirement.
```
$ curl http://192.168.159.131:3000/service-boilerplate/readiness -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 3000 (#0)
> GET /service-boilerplate/readiness HTTP/1.1
> Host: 192.168.159.131:3000
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sat, 02 May 2020 20:04:23 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```
#### `/startup` for startup probe
This is a special route and can be used for defining startup probe. This route will always respond back with response code `200`. This route can be tuned more according to the service component requirement.
```
$ curl http://192.168.159.131:3000/service-boilerplate/startup -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 3000 (#0)
> GET /service-boilerplate/startup HTTP/1.1
> Host: 192.168.159.131:3000
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sat, 02 May 2020 20:07:27 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```

## Adding new routes to your service-component
All routes are defined in `<service-component-dir>/routes/routes.js`. There are three pre-defined routes present in the `routes.js` 
```
/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',(req,res)=> {
    res.status(200);
    res.send();
});

// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(req,res)=> {
    res.status(200);
    res.send();
});

// Protect slow starting containers with startup probes
router.get('/startup',(req,res)=> {
    res.status(200);
    res.send();
});

```
You can add new routes in the same file

## Defining Service Configuration
All service configuration are defined in `<projecr-root>/.env.example` file. This file is for reference purposes only and all new configurations should be defined as a container environment variable.

Configuration is explicitly mapped to a java script object in `<project-dir>/config/appconfig.js` file using following code:
```
module.exports = {
    env: process.env.NODE_ENV,
    port: process.env.OFL_MED_PORT,
    contextRoot: process.env.OFL_MED_CONTEXT_ROOT
}
```

All new configuration should be defined in the same way.

## Building and Running Docker Images
This application comes with built-in Docker support and it is built grounds up on docker container. Along with the container engine, application is also tested on Kubernetes cluster. 

### Building Docker image
Execute following command to built docker image:
```
$ docker build -t service-boilerplate-api .
Sending build context to Docker daemon  119.3kB
Step 1/17 : ARG NODE_VERSION=12.16.2-slim
Step 2/17 : FROM node:${NODE_VERSION}
 ---> 143b043e4cff
Step 3/17 : LABEL "authur"="openfintechlab.com"       "source-repo"="https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api"       "copyright"="Copyright 2020-2022 Openfintechlab, Inc. All rights reserved."
 ---> Using cache
 ---> 5b81c5b0620a
Step 4/17 : ARG NODE_ENV=production
 ---> Using cache
 ---> 42b93b82e895
Step 5/17 : ENV NODE_ENV $NODE_ENV
 ---> Using cache
 ---> c33cfa4cc60e
Step 6/17 : ARG PORT=3000
 ---> Using cache
 ---> 832ea92d9dbb
Step 7/17 : ENV PORT $PORT
 ---> Using cache
 ---> 35903fc26c2d
Step 8/17 : RUN npm i npm@latest -g
 ---> Using cache
 ---> fc28161d0339
Step 9/17 : RUN mkdir /opt/node_app && chown node:node /opt/node_app
 ---> Using cache
 ---> ea3a0794e5ba
Step 10/17 : WORKDIR /opt/node_app
 ---> Using cache
 ---> 7d4d105c6df0
Step 11/17 : USER node
 ---> Using cache
 ---> 9f75543a299c
Step 12/17 : COPY package.json package-lock.json* ./
 ---> 8ebb23ce0ac4
Step 13/17 : RUN npm install --no-optional && npm cache clean --force
 ---> Running in 0c8c1c6c2bb7
added 95 packages from 60 contributors and audited 334 packages in 1.898s
found 0 vulnerabilities

npm WARN using --force I sure hope you know what you are doing.
Removing intermediate container 0c8c1c6c2bb7
 ---> a25c82682e83
Step 14/17 : ENV PATH /opt/node_app/node_modules/.bin:$PATH
 ---> Running in 4235aa1654f5
Removing intermediate container 4235aa1654f5
 ---> 8001ba120dd7
Step 15/17 : WORKDIR /opt/node_app/app
 ---> Running in fa601bdcde6f
Removing intermediate container fa601bdcde6f
 ---> e22d96b8882c
Step 16/17 : COPY . .
 ---> 7c1224bf3aa1
Step 17/17 : CMD [ "node", "./index.js" ]
 ---> Running in 24a121e17631
Removing intermediate container 24a121e17631
 ---> 01df02d71b03
Successfully built 01df02d71b03
Successfully tagged service-boilerplate-api:latest
```

### Building the image with different base image
You can provide build arguments to the `docker build` commands to pass following variables:
1. `NODE_VERSION` : Base image (exp; 12.16.2-slim) 
2. `NODE_ENV` : Node run mode (e.g. 'production')
3. `PORT` : Port which will be exposed by the container.
> NOTE: This variable is different from the environment variable used in the application configuration

Example, following command will build the container using `13.14.0-slim` as a base image tag:
```
$ docker build --build-arg NODE_VERSION=13.14.0-slim -t  service-boilerplate .
Sending build context to Docker daemon  121.9kB
Step 1/17 : ARG NODE_VERSION=12.16.2-slim
Step 2/17 : FROM node:${NODE_VERSION}
13.14.0-slim: Pulling from library/node
b248fa9f6d2a: Already exists 
dffc92453adc: Already exists 
db284201f3ba: Pull complete 
b39ba9d9f5e0: Pull complete 
3deb7f9802b7: Pull complete 
Digest: sha256:11bd13bd8b4d2b812059ffa9eb1db941057c7994938f6c7c66fe1d73278c876f
Status: Downloaded newer image for node:13.14.0-slim
 ---> a236ce6370c5
Step 3/17 : LABEL "authur"="openfintechlab.com"       "source-repo"="https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api"       "copyright"="Copyright 2020-2022 Openfintechlab, Inc. All rights reserved."
 ---> Running in d4f8ccce0569
 .......................................
 .......................................
 .......................................
 .......................................
```

### Running on kubernetees

```
$ kubectl apply -f k8s-pod.yaml 
pod/service-boilerplate created
service/service-boilerplate created
$ kubectl get po
NAME                  READY   STATUS    RESTARTS   AGE
service-boilerplate   1/1     Running   0          34s
$ kubectl get service -o wide
NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE   SELECTOR
kubernetes            ClusterIP   10.152.183.1     <none>        443/TCP          47h   <none>
service-boilerplate   NodePort    10.152.183.157   <none>        3000:32392/TCP   76s   run=service-boilerplate
```
You can test the api using following command:
```
$ curl http://192.168.159.131:32392/service-boilerplate/healthz -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 32392 (#0)
> GET /service-boilerplate/healthz HTTP/1.1
> Host: 192.168.159.131:32392
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sun, 03 May 2020 10:58:42 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```


# Component Specifications

## Directory Structure
service-boilerplate-api/
├── bin
│   ├── config
│   │   ├── AppConfig.d.ts
│   │   ├── AppConfig.js
│   │   └── AppConfig.js.map
│   ├── index.d.ts
│   ├── index.js
│   ├── index.js.map
│   ├── mapping
│   │   └── bussObj
│   │       ├── Response.d.ts
│   │       ├── Response.js
│   │       └── Response.js.map
│   ├── routes
│   │   ├── routes.d.ts
│   │   ├── routes.js
│   │   └── routes.js.map
│   └── utils
│       ├── ExpressApp.d.ts
│       ├── ExpressApp.js
│       ├── ExpressApp.js.map
│       ├── Logger.d.ts
│       ├── Logger.js
│       └── Logger.js.map
├── CHANGELOG.md
├── Dockerfile
├── docs
│   └── readme.md
├── k8s-pod-dev.yaml
├── k8s-pod.yaml
├── LICENSE.md
├── package.json
├── README.md
├── skaffold.yaml
├── src
│   ├── config
│   │   └── AppConfig.ts
│   ├── index.ts
│   ├── mapping
│   │   ├── bussObj
│   │   │   ├── Enttity.ts.txt
│   │   │   └── Response.ts
│   │   ├── EnttityController.ts.txt
│   │   └── simulator
│   │       └── simulator.ts.txt
│   ├── routes
│   │   └── routes.ts
│   ├── tests
│   │   └── JestTestCase.ts.text
│   └── utils
│       ├── ExpressApp.ts
│       └── Logger.ts
├── tsconfig.json
└── yarn.lock


## Built With
* [Skaffold](https://skaffold.dev/) - Local kubernetes development
* [Node](https://nodejs.org/en/) - Unddeline code framework
* [Docker](https://www.docker.com/) - Container build engine

## Libraries used

| Framework                   | Reason                                           |
|-----------------------------|:-------------------------------------------------|   
| express                     | Underline web application framework              |
| dotenv                      | Framework for loading environment variables      |
| cors                        | Cross Origin Resource Sharing configuration      |
| helmet                      | Framework for adding security headers            |
| http-status                 | Utility to interact with the status codes        |
| log4js                      | Underline logging framework                      |
| morgan                      | HTTP Request logger middleware for nodejs        |
| xss-clean                   | Middleware to sanitized user input               |
| nodemon (dev)               | Automatic file monitoring and reloading util     |
| typescript                  | Application is developed in typescript           |
| yarn                        | yarn modules for managing modules                |

## Authors
* **Muhammad Furqan** - *Initial work* - [Openfintechlab.com](https://openfintechlab.com)

