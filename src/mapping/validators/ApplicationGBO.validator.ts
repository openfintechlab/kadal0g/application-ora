import Joi                              from "joi";
import logger                           from "../../utils/Logger"
import {PostRespBusinessObjects}        from "../bussObj/Response";
import AppConfig                        from "../../config/AppConfig";

export default class AppplicationGBOValidator{
    ApplicationSchemaModel = Joi.object({
        "application": {
            "application_id":   Joi.string().min(8).max(56).required(),
            "name":             Joi.string().min(8).max(128).required(),
            "status":           Joi.string().min(1).max(4).required(),
            "status_description":       Joi.string().min(1).max(64)
        }
    });

    /**
     * Validates the JSON object and validates is against the schema
     * @param {any} request Request to validate
     */
    public vlaidate(request:any){
        let {error,value} = this.ApplicationSchemaModel.validate(request,{ presence: "required" });
        if(error){
            logger.error(`Error while validating the business object: ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    private getValidationError(error:any){
        if(AppConfig.config.OFL_MED_NODE_ENV === 'debug'){
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error", trace);   
        }else{
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error");   
        }           
    }
}


/**
 * application_id  VARCHAR2(56 CHAR) NOT NULL,
            srv_id          VARCHAR2(56 CHAR) NOT NULL,
            version_id      VARCHAR2(56 CHAR) NOT NULL,
            endpoint_id     VARCHAR2(56 CHAR) NOT NULL,
            created_on      TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
            updated_on      TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
            created_by      VARCHAR2(64)
 * 
 */