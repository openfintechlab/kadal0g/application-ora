
import OracleDBInteractionUtil          from "..//utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import AppConfig                        from "..//config/AppConfig";


export default class ApplicationController{
    /**
     * Value of to can not be greater then this value. 
     */
    private readonly _MAX_RECORD_ABUSE:number = 100;    

    SQLStatements = {
        "INSERT_SQL01_INTO_MEDSRVAPP": "INSERT INTO med_service_application (application_id,name,status,status_description,created_on,updated_on,created_by,updated_by) VALUES (:v0,:v1,:v2,:v3,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SYSTEM','SYSTEM')",
        "SELEC_SQL02_SELECTONID": "SELECT APPLICATION_ID FROM MED_SERVICE_APPLICATION WHERE APPLICATION_ID=:v1 OR NAME=:v2",
        "SELECT_SQL03_SELECTALL": "select RES.*,(SELECT COUNT(*) FROM med_service) as TTL_ROWS from ( " +
                                    "SELECT application_id,name,status,status_description,created_on,updated_on,created_by,updated_by FROM med_service_application " +
                                    ") RES where ROWNUM BETWEEN :v1 and :v2 order by rownum",
        "SELECT_SQL04_SELECT_ON_APPID": "SELECT application_id,name,status,status_description,created_on,updated_on,created_by,updated_by FROM med_service_application where application_id=:v1",
        "DELETE_SQL05_DELETE_ON_APPID": "DELETE FROM MED_SERVICE_APPLICATION WHERE APPLICATION_ID=:v1",
        "UPDATE_SQL06_UPDATE_ON_APPID": "UPDATE med_service_application SET name = :v1,status = :v2,status_description = :v3, updated_on= CURRENT_TIMESTAMP,updated_by=CURRENT_TIMESTAMP WHERE    application_id = :v4"
    }

    constructor(private referenceID:string){}

    /**
     * Get All applications definition from the database
     * @param {number} from From index
     * @param {number} to To index
     */
    public async getAll(from:number=1,to:number=10){
        logger.info(`${this.referenceID}: Getting all applications from: ${from} - to: ${to}`);
        to = (to > this._MAX_RECORD_ABUSE)? this._MAX_RECORD_ABUSE: to;                    
        if(from > to){
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8155","Invalid input");
        }
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL03_SELECTALL, [from,to]);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            return await this.createBusinessObject(result);
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8151","Error While performing the Operation");
        }
    }

    /**
     * Get specific application from the database
     * @param {string} application_id Application ID 
     */
    public async get(application_id:string){
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL04_SELECT_ON_APPID, [application_id]);
            return await this.createBusinessObject(result);
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            if(error.metadata === undefined){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8151","Error While performing the Operation");   
            }else{
                throw error;
            }
        }
    }

    public async delete(application_id:string){
        try{
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.DELETE_SQL05_DELETE_ON_APPID,[application_id]);
            logger.debug(`${this.referenceID} Result of DB deletion: ${util.inspect(dbResult,{compact:true,colors:true, depth: null})} `);
            if(dbResult.rowsAffected > 0){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");   
            }                
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");   
            }
        }catch(error){
            logger.error(`${this.referenceID} Error adding applicattion in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * Add application in the database server
     * @param {any} request JSON object
     */
    public async create(request:any){
        logger.debug(`[${this.referenceID}]: Request Received for creating application: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELEC_SQL02_SELECTONID, [request.application.application_id, request.application.name]);
            if(result.rows.length > 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8152","Application already exist", new PostRespBusinessObjects.Trace("error-desc","Applicatoin with same name or ID already exist"));
            }
            let binds = [
                request.application.application_id,
                request.application.name,
                request.application.status,
                request.application.status_description,

            ];
            await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.INSERT_SQL01_INTO_MEDSRVAPP,binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`${this.referenceID} Error adding applicattion in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * Updates application in the database server
     * @param {any} request JSON object
     */
    public async update(request:any){
        logger.debug(`[${this.referenceID}]: Request Received for updating application: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL04_SELECT_ON_APPID, [request.application.application_id]);
            if(result.rows.length <= 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");
            }
            let binds = [                
                request.application.name,
                request.application.status,
                request.application.status_description,
                request.application.application_id
            ];
            const respObj = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.UPDATE_SQL06_UPDATE_ON_APPID,binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`${this.referenceID} Error adding applicattion in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    private async createBusinessObject(dbResult:any){
        return new Promise<any>((resolve:any, reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    "trace": [
                        {
                            "source": "TotalRows",
                            "description": dbResult.rows[0].TTL_ROWS
                        }
                    ]
                },
                "application": []
            };
            dbResult.rows.forEach((row:any) => {
                bussObj.application.push({
                    "application_id": row.APPLICATION_ID,
                    "name": row.NAME,
                    "status": row.STATUS,
                    "status_description": row.STATUS_DESCRIPTION
                });
            });
            resolve(bussObj);
        });
    }
}