/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * <b>Description</b>
 * - Root entry level file for bootstarting node js application
 * - Load configuration from configuration manager service
 * - Retries configuration loading for (hardcoded) number of frequency
 */
import express                  from 'express';
import chalk                    from "chalk";
import helmet                   from "helmet"
import morgan                   from "morgan";

import ExpressApp               from "./utils/ExpressApp"
import router                   from "./routes/routes";
import logger                   from "./utils/Logger";
import AppConfig                from "./config/AppConfig";
import {AppHealth}              from "./config/AppConfig";
import util                     from "util";
import OracleDBInteractionUtil  from './utils/OracleDBInteractionUtil';

/**
 * Main entry point class
 */
export abstract class Main{
    private static app:ExpressApp;
    private static server:any;
    
    /**
     * Fetch configuration from the service config and start the listener
     * <b>NOTE:</b> 
     * * This procedure requires environment variable OFL_MED_SRVCONFIG_URI preloaded </br>
     * for fetching service configurations.
     * * In-case service configuration is not found, this procedure will retry in predefined </br>
     * frequency of 3
     */
    public static async start(){
        this.displayBanner();
        const retryCount = 3;
        let count = 0;
        const sleep = util.promisify(setTimeout);
        // Retry 3 times in-case un-able to fetch configuration from config manager
        for(count=0; count < retryCount; count ++){
            try{
                await AppConfig.loadConfigurationfromConfigService(AppConfig.service_config_uri, AppConfig.package_name);
                Main.app = new ExpressApp({
                    contextRoot: AppConfig.contextRoot,    
                    port: AppConfig.config.OFL_MED_PORT,
                    middlewares: [
                        helmet(),
                        express.json(),
                        express.urlencoded({ extended: true }),
                        morgan('combined')
                    ],
                    router: router    
                });
                this.startAppListener();                
                return;
            }catch(error){
                logger.error(`Fatal Error while fetching configuration. Retrying (${count+1}/${retryCount})`);                
                if(error.response !== undefined ){
                    logger.error(`Error Detail: Status Code: ${chalk.bold.redBright(error.response.status)} | ${chalk.bold.red('Response headers:')} ${util.inspect(error.response.headers,{compact:true,colors:true, depth: null})} | ${chalk.bold.red('Response Message:')} ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})} | ${chalk.bold.red('Response configuration:')} ${util.inspect(error.response.config,{compact:true,colors:true, depth: null})}`);
                }else{
                    logger.error(`Error Trace: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
                }                                
                await sleep(15*1000); 
                if(count + 1 === retryCount){
                    throw new Error("Unable to load configuration after retrial frequency is exhausted");
                }
            }   

        }        
    }

    
    /**
     * Displays banner information of the sevice component using logger
     */
    public static displayBanner(){
        logger.info(chalk.yellow("-----------------------------------"))
        logger.info(chalk.bold.yellow("Medulla - The Financial Middleware"));
        logger.info(chalk.bold.yellow("Copyright @ Openfintechlab.com"));
        logger.info(chalk.yellow("-----------------------------------"))
        logger.info(chalk.cyan("Starting Application"));
    }

    /**
     * Starts the express application listener
     */
    private static async startAppListener(){
        this.server = this.app.listen();      
        AppHealth.express_loaded = true;  
    }
    
    public static get expressApp(){
        return this.app;
    }
    
}

process.once('exit', (_) => async function() {    
    logger.info("[EXIT] Existing the app");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.server.close();
        process.exit(1);   
    }
});

process.once('SIGINT', async function() {
    logger.info("[SIGINT] Disconnecting all listeners");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.server.close();
        process.exit(1);   
    }
});

process.once('SIGTERM', async function() {
    logger.info("[SIGTERM] Disconnecting all listeners");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.server.close();
        process.exit(1);   
    }
        
});

process.once('SIGHUP', async function() {
    logger.info("[SIGHUP] Disconnecting all listeners");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.server.close();
        process.exit(1);   
    }
});

/**
 * Bootstarting express application
 */
const load = async() => {
    try{
        await Main.start();            
        await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
        await AppConfig.startConfigLoaderPoolAgent();        
    }catch(error){
        logger.error(`Error received while Bootstarting the solution: ${error}`);
        logger.error(`Unable to proceed. Closing application`);       
        process.exit(1);
    }    
}; if(!process.env.OFL_MED_UNITTEST) {load();} /* Kick Start */
