/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * <b>Description</b>
 * - Inetaction utility for connecting with Oracle Database server
 * <b>Reference</b>
 * - https://github.com/oracle/node-oracledb/blob/master/examples/connectionpool.js
 */

import AppConfig    from "../config/AppConfig";
import {AppHealth}  from "../config/AppConfig";
import logger       from "../utils/Logger";
import oracledb     from "oracledb";


 /**
  * Utility class for interacting with OracleDatabase Sever
  * @abstract
  */
 export default abstract class OracleDBInteractionUtil{

    private static _MAX_POOL_CONNECTIONS = 10;  
    private static _CONNECTION_INITIATED = false;
    
    /**
     * Initiate pool connection by fetching configuration from the database
     */
    public static async initPoolConnectionsWithConfig(){
        logger.debug(`Initializing Connection Pool with Config from Config Store`);
        logger.debug(`Configurations: DB User: ${AppConfig.config.OFL_MED_DB_USER} DB Connection String: ${AppConfig.config.OFL_MED_DB_CONNSTR}`);
        if(!(AppConfig.config.OFL_MED_DB_USER &&
            AppConfig.config.OFL_MED_DB_PASS &&
            AppConfig.config.OFL_MED_DB_CONNSTR)){
                this._CONNECTION_INITIATED = false;
                throw Error("Mandatory configuration not found required to connect to the database server");
            }else{
                try{
                    await this.initPoolConnections(
                        AppConfig.config.OFL_MED_DB_USER,
                        AppConfig.config.OFL_MED_DB_PASS,
                        AppConfig.config.OFL_MED_DB_CONNSTR                        
                    );
                }catch(error){                    
                    throw error;
                }                
            }
    }  

    /**
     * Initialize pool connection with the database
     * @param dbUser (string) Database user
     * @param dbPass (string) Database password
     * @param dbConnString (string) Database connection string
     * @param maxPool (number) Maximum pool size
     * @param minPool (number) Minimum Pool size
     * @param poolIncrement (number) Pool increment frequency
     */
    public static async initPoolConnections(dbUser: string, dbPass: string, dbConnString: string, maxPool = 10, minPool = 3, poolIncrement = 1){
        logger.info("Initializing the db connection")        
        try{
            await oracledb.createPool({
                user: dbUser,
                password: dbPass,
                connectionString: dbConnString,
                poolMax: maxPool,
                poolMin: minPool,
                poolIncrement: poolIncrement        
            });
            logger.info(`Pooled connection started maxPoolConn: ${maxPool}, minPoolConn: ${minPool}, poolIncrement: ${poolIncrement}`);
        }catch(error){            
            logger.error(`Error Received while initiating connection with database ${error}`);
            throw error;
        }                
    }
    
    /**
     * Get DB Connection from this procedure
     * NOTE: please call connection.close() to put the connection back to the pool
     */
    public static async getDBConnection(){
        let connection:oracledb.Connection;        
        connection = await oracledb.getConnection();
        return connection;
    }

    /**
     * Execute SQL SELECT statement on the database server.      
     * @example 
     * //Option#1: Executing SQL statement with a parameter </br>
     * let sql:string = `SELECT sysdate FROM dual `;
     * let binds:any = [1];
     * OracleDBInteractionUtil.executeRead(sql);     
     * //Option#2: Execute SQL statement without a paramter
     * let sql:string = `SELECT sysdate FROM dual WHERE :b = 1`;
     * const binds = [1];
     * OracleDBInteractionUtil.executeRead(sql,binds);
     * @param sqlStatement (string)* SQL Statement to execute
     * @param binds (string[]) Optional- Array of string, number, date to replace the bind variable
     */
    public static async executeRead(sqlStatement:string, binds?: any){        
        const options = {outFormat: oracledb.OUT_FORMAT_OBJECT};
        return await this.execute(sqlStatement,options,binds);
    }


    /**
     * Execute SQL INSERT / UPDATE / DELETE statement on the database server
     * @example
     * // Option#1: Execute SQL statement without any variable
     * let sql:string = 'DROP TABLE test';
     * OracleDBInteractionUtil.executeUpdate(sql);     
     * // NOTE: Option 1 will not work for UPDATE INSERT and DELETE statements. Should provide bind variable for these operations.
     * // Option#2: Execute SQL statement with variable
     * let sql:string = "DELETE FROM test WHERE COL1=:a"";
     * let binds = ['TEST']
     * OracleDBInteractionUtil.executeUpdate(sql,binds);          
     * @param sqlStatement (string) SQL statement to execute
     * @param binds (string[]) Optional - Array of string, number, date to replace the bind variable
     */
    public static async executeUpdate(sqlStatement:string, binds?:any){
         // For a complete list of options see the documentation.
        const options = {
           autoCommit: true,
           // batchErrors: true,  // continue processing even if there are data errors
           bindDefs: [
             { type: oracledb.NUMBER },
             { type: oracledb.STRING, maxSize: 20 }
           ]
         };  
        return await this.execute(sqlStatement,options,binds);
    }

    private static async execute(sqlStatement:string,options:any, binds?: any ){
        let connection!:oracledb.Connection;        
        try{
            connection = await oracledb.getConnection();                    
            let result: any;
            if(binds){
                result = await connection.execute(sqlStatement,binds,options);
            }else{
                console.log("In result");
                result = await connection.execute(sqlStatement);
            }            
            return result;
        }catch(error){
            logger.debug(`Error while executing sql statement Error: ${error}`);
            logger.error(`Error while executing statement: ${sqlStatement} Error: ${error}`);
            throw new Error(error);
        }finally{
            try{
                if(connection){
                    await connection.close();
                    logger.info("Connection put back to the pool")
                }
            } catch(error) {
                logger.info(`Error while closing connection ${error}`);
            }
        }
    }

    /**
     * Close pool connection with the database
     */
    public static async closePoolAndExit(){
        try{
            await oracledb.getPool().close();
            logger.info("Pool Connection closed");
        }catch(error){
            logger.error(`Error while closing connection ${error}`);
        }
    }

 }