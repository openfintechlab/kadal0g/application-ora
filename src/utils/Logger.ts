/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Logger utility class
 */
import log4js       from "log4js";
import AppConfig    from "../config/AppConfig"; 


const logger = log4js.getLogger();
// Change to AppConfig.config.NNASA
// logger.level = (AppConfig === undefined )?'debug': AppConfig.log_level;   
if( process.env.OFL_MED_NODE_ENV){
    logger.level =  process.env.OFL_MED_NODE_ENV
}else{
    logger.level = 'debug';
}
logger.debug('Logger initiated...');

export default logger;