import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";
import * as _                   from "lodash";
import UTSetup from "./setup_express";


describe(`PUT Endpoint Vlaidation Test Cases`, () => {
    const post_message = require(`./POST.message.json`);    
    const put_message = require(`./PUT.message.json`);    
    const delete_message = require(`./DELETE.message.json`);


    beforeAll(async () => {        
        try{
            logger.info(`Package Name: ${AppConfig.package_name}`)
            await Main.start();            
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
            
             // Create a record in the database
             // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
             const url = UTSetup.getURL();
             logger.info(`Calling URL: ${url}`);
             const response = await axios({
                 "method": "post",
                 "url": url,
                 "timeout": 5 * 1000, // timeout in milliseconds
                 "headers": {"Content-Type": "application/json"},
                 "data": post_message
             });
             const statusCode:number = response.status;
             expect(statusCode === 200 || statusCode === 201).toBeTruthy();  

        }catch(error){                        
            logger.error(`Error received while Bootstarting the solution: ${error}`);            
            logger.error(`Unable to proceed. Closing application`);       
            process.exit(1);
        }            
    });

    afterAll(async ()=> {
        // Delete specific record
        try{
            // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/${delete_message.id}`
            const url = UTSetup.getURL() + `/${delete_message.id}`;
            logger.info(`+ Cleaning created records using url: ${url}`);
            const response = await axios.delete( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
            }else{
                logger.error(chalk.red(`Error while cleaning specific record`))
            }            
        }
        await OracleDBInteractionUtil.closePoolAndExit(); 
        await Main.expressApp.server.close();
    });

    test(`PUT / UPDATE ${AppConfig.package_name} to the backend service-plane`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        const url = UTSetup.getURL();        
        logger.info(`Calling URL: ${url}`);
        try{    
            const response = await axios({
                "method": "put",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": put_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy();            
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).not.toBe(500);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`GET the records and compare fields ${AppConfig.package_name} to the backend service-plane`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/${delete_message.id}`
        const url = UTSetup.getURL() + `/${delete_message.id}`;
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201 || statusCode === 404).toBeTruthy();
            // TLDR; Compare all fields            
            expect(_.isEqual(response.data.application[0],put_message.application)).toBeTruthy();
            // END;
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).not.toBe(404);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`PUT / UPDATE unwknown ${AppConfig.package_name} to the backend service-plane. Should receive error 404`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        const url = UTSetup.getURL();
        logger.info(`Calling URL: ${url}`);
        put_message.application.application_id = 'should_not_exist';
        try{    
            const response = await axios({
                "method": "put",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": put_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).not.toBeTruthy();            
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(404);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    
});