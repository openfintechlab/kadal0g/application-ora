import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";


export default class UTSetup{

    public  static readonly _ENV_VAR_HOST:string = "unit_test_host";

    public static getURL():string{
        let url:string = '';
        // http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}
        if(process.env[UTSetup._ENV_VAR_HOST]){
            // export unit.test.host = 'http://localhost:3000'
            url = process.env[UTSetup._ENV_VAR_HOST] + `/${AppConfig.package_name}`;
        }else{
            url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        }
        return url;
    }
}