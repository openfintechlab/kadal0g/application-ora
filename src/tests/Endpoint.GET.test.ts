import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";
import UTSetup                  from "./setup_express";




describe(`GET Endpoint Vlaidation Test Cases`, () => {
    
    beforeAll(async () => {        
        try{
            logger.info(`Package Name: ${AppConfig.package_name}`)
            await Main.start();            
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
            
        }catch(error){
            logger.error(`Error received while Bootstarting the solution: ${error}`);
            logger.error(`Unable to proceed. Closing application`);       
            process.exit(1);
        }            
    });
    
    afterAll(async ()=> {
        await OracleDBInteractionUtil.closePoolAndExit(); 
        await Main.expressApp.server.close();
    });

    test(`Fetch all ${AppConfig.package_name} from the backend service-plane`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
        const url = UTSetup.getURL();
        logger.info(`Calling URL: ${url}`);
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201 || statusCode === 404).toBeTruthy();
        }catch(error){
            if(error.response){
                logger.info(chalk.yellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(404);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`Fetch specific ${AppConfig.package_name} from the backend service-plane`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/should_not_exist`
        const url = UTSetup.getURL() + '/shoudl_not_exist';
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201 || statusCode === 404).toBeTruthy();
        }catch(error){
            if(error.response){
                logger.info(chalk.yellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(404);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`Fetch ${AppConfig.package_name} from invalid backend service-plane`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/should_not_exist`
        const url = UTSetup.getURL() + '/shoudl_not_exist';
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201 || statusCode === 404).not.toBeTruthy();
        }catch(error){
            if(error.response){
                logger.info(chalk.bgRedBright(`${error.response.status} .::. ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                let statusCode:number = error.response.status;
                expect(statusCode === 500 || 406).toBeTruthy()
            }else{                
                fail(error.message)                
            }            
        }        
    });

});