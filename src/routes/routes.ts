/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                          from "express";
import {AppHealth}                      from "../config/AppConfig";
import logger                           from "../utils/Logger";
import util                             from "util";
import {PostRespBusinessObjects}        from "../mapping/bussObj/Response";
import AppplicationGBOValidator         from "../mapping/validators/ApplicationGBO.validator";
import ApplicationController            from "../mapping/ApplicationController";
import { v4 as uuidv4 }                 from "uuid";

const router: any = express.Router();


/**
 * Get all applications MAXIMUM=100 
 * param1 : ?from={startNumber}
 * param2 : ?to={toNumber}
 */
router.get(`/`, async(request:any, response:any) => {
    let transid:string = uuidv4();
    logger.info(`${transid}: Procedure called for fetching all applications`);
    response.set("Content-Type","application/json; charset=utf-8");    
    let from:number, to:number;
    from = (request.query.from)? request.query.from : undefined;
    to = (request.query.to)? request.query.to : undefined;
    try{
        let srvResp = await new ApplicationController(transid).getAll(from,to);
        response.status(200).send(srvResp);
    }catch(error){
        logger.debug(`Error occured while fetching all records  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        } 
    }

    // response.status(503).send();
});

/**
 * Get specific application
 */
router.get(`/:app_id`, async(request:any, response:any) => {
    let transid:string = uuidv4();
    logger.info(`${transid}: Procedure called for fetching specific applications`);
    response.set("Content-Type","application/json; charset=utf-8");    
    let id:string = request.params.app_id;
    try{
        let result = await new ApplicationController(transid).get(id);
        response.status(200).send(result);
    }catch(error){
        logger.debug(`Error occured while fetching all records  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }
    }
});

/**
 * Create new application
 */
router.post(`/`, async(request:any, response:any)=>{
    let transid:string = uuidv4();
    logger.debug(`${transid}: Calling http-post for posting data: ${request.data}`);
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`${transid}: Procedure called for creating application`);      
    try{        
        let parsedObj = new AppplicationGBOValidator().vlaidate(request.body);
        logger.debug(`${transid}: Parsed response from validation: ${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        const resBO = await new ApplicationController(transid).create(parsedObj);
        response.status(201).send(resBO);
    }catch(error){
        logger.debug(`${transid}: Error occured while creating application:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
    
});

/**
 * Update specific application
 */
router.put(`/`, async(request:any, response:any)=>{
    let transid:string = uuidv4();
    logger.debug(`${transid}: Calling http-post for posting data: ${request.data}`);
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`${transid}: Procedure called for creating application`);      
    try{        
        let parsedObj = new AppplicationGBOValidator().vlaidate(request.body);
        logger.debug(`${transid}: Parsed response from validation: ${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        const resBO = await new ApplicationController(transid).update(parsedObj);
        response.status(201).send(resBO);
    }catch(error){
        logger.debug(`${transid}: Error occured while creating application:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)        
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

/**
 * Delete specific application
 */
router.delete(`/:app_id`, async(request:any, response:any)=>{
    let transid:string = uuidv4();
    logger.debug(`${transid}: Calling http-delete for deleting application with id: ${request.params.app_id}`);
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`${transid}: Procedure called for deleting application`);      
    try{        
        let resBO = await new ApplicationController(transid).delete(request.params.app_id)
        response.status(201).send(resBO);
    }catch(error){
        logger.debug(`${transid}: Error occured while deleting application:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)        
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});


/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
 router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && !AppHealth.reload_required)
        res.status(200);    
    else{
        logger.error(`Health check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);    
    }
        
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded)
        res.status(200);    
    else{
        logger.error(`Readiness check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);        
    }        
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && AppHealth.express_loaded){
        res.status(200);    
    }else{
        logger.error(`Startup check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);        
        res.status(503);    
    }        
    res.send();
});


export default router;